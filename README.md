# I18n Plugin

Conjunto de utilidades para el correcto funcionamiento de un web multilingüe

## Routes

El ruteado otorga una class Route que se encarga de colocar o leer correctamente los prefijos de los idiomas:

* El correcto funcionamiento de las urls con un prefijo de idioma de dos letras
* El correcto funcionamiento de las URLS escritas por funciones como Router::url() o Controller::redirect()

Para hacer funcionar el ruteado es necesario lo siguiente:

* Para asignarla de forma globan a todas las rutas que se describan, es necesario indicar en routes.php lo siguiente:
```php
Router::defaultRouteClass( 'I18n.I18nRoute');
```

* Para asignarla a las rutas de los plugins, en routes.php del plugin:
```php
Router::plugin( 'PluginName',  function( $routes) {
  $routes->routeClass( 'I18n.I18nRoute');
  $routes->fallbacks();
});
```
* Para asignarla a una ruta concreta:
```php
Router::connect( '/noticias', ['controller' => 'articles'], ['routeClass' => 'I18n.18nRoute']);
```

Para que los ruteados de los plugins funcionen correctamente es necesario poner Plugin::routes(); antes de la definición de otras rutas.