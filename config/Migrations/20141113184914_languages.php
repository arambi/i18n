<?php

use Phinx\Migration\AbstractMigration;

class Languages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
      $languages = $this->table( 'languages');
      $languages->addColumn( 'name', 'string', ['limit' => 50])
            ->addColumn( 'iso2', 'string', ['limit' => 6, 'null' => false])
            ->addColumn( 'iso3', 'string', ['limit' => 3, 'null' => false])
            ->addColumn( 'locale', 'string', ['limit' => 6, 'null' => false])
            ->addColumn( 'by_default', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn( 'position', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addColumn( 'modified', 'datetime', array('default' => null))
            ->addIndex( ['iso2'])
            ->addIndex( ['iso3'])
            ->save();


      $i18n = $this->table( 'i18n');
      $i18n->addColumn( 'locale', 'string', ['limit' => 5])
            ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
            ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => false])
            ->addColumn( 'field', 'string', ['limit' => 64, 'null' => false])
            ->addColumn( 'content', 'text', ['default' => null, 'null' => true])
            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addColumn( 'modified', 'datetime', array('default' => null))
            ->addIndex( ['locale', 'model', 'foreign_key', 'field'], ['unique' => true])
            ->addIndex( ['locale', 'model', 'foreign_key'], ['unique' => false])
            ->addIndex( ['locale', 'model'], ['unique' => false])
            ->addIndex( ['model', 'foreign_key', 'field'], ['unique' => false])
            ->addIndex( ['model', 'foreign_key'], ['unique' => false])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'languages');
      $this->dropTable( 'i18n');
    }
}