<?php

use Phinx\Migration\AbstractMigration;

class Dictionaries extends AbstractMigration
{
  /**
   * Change Method.
   *
   * Write your reversible migrations using this method.
   *
   * More information on writing migrations is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
   *
   * The following commands can be used in this method and Phinx will
   * automatically reverse them when rolling back:
   *
   *    createTable
   *    renameTable
   *    addColumn
   *    renameColumn
   *    addIndex
   *    addForeignKey
   *
   * Remember to call "create()" or "update()" and NOT "save()" when working
   * with the Table class.
   */
  public function up()
  {
    $dictionaries = $this->table( 'dictionaries');
    $dictionaries 
      ->addColumn( 'msgid', 'string', ['limit' => 255])
      ->addColumn( 'msgstr', 'text', ['default' => null, 'null' => true])
      ->addColumn( 'domain', 'string', ['limit' => 16])
      ->addColumn( 'msgid_plural', 'boolean', ['null' => true, 'default' => 0])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->save();

    $i18n = $this->table( 'dictionaries_i18n');
    $i18n
      ->addColumn( 'locale', 'string', ['limit' => 5])
      ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
      ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => false])
      ->addColumn( 'field', 'string', ['limit' => 64, 'null' => false])
      ->addColumn( 'content', 'text', ['default' => null, 'null' => true])
      ->addColumn( 'created', 'datetime', array('default' => null))
      ->addColumn( 'modified', 'datetime', array('default' => null))
      ->addIndex( ['locale', 'model', 'foreign_key', 'field'], ['unique' => true])
      ->addIndex( ['locale', 'model', 'foreign_key'], ['unique' => false])
      ->addIndex( ['locale', 'model'], ['unique' => false])
      ->addIndex( ['model', 'foreign_key', 'field'], ['unique' => false])
      ->addIndex( ['model', 'foreign_key'], ['unique' => false])
      ->save();

  }

  public function down()
  {
    $this->dropTable( 'dictionaries');
    $this->dropTable( 'dictionaries_i18n');
  }
}
