<?php

use Phinx\Migration\AbstractMigration;

class LanguagesPosition extends AbstractMigration
{

  public function up()
  {
    $languages = $this->table( 'languages');
    $languages->changeColumn( 'position', 'integer', ['limit' => 4, 'null' => true, 'default' => null])->save();
  }
}
