<?php

use Migrations\AbstractMigration;

class WebCountries extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {

    if (!$this->hasTable('i18n_web_countries')) {
      $this->table('i18n_web_countries')
        ->addColumn('title', 'string', ['null' => true, 'default' => null])
        ->addColumn('code', 'string', ['null' => true, 'default' => null])
        ->addColumn('published', 'boolean', ['null' => false, 'default' => 1])
        ->addColumn('position', 'integer', ['null' => true, 'default' => null])
        ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
        ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
        ->addIndex('published')
        ->create();
    }

    if (!$this->hasTable('i18n_web_countries')) {
      $this->table('i18n_web_countries_translations', ['id' => false, 'primary_key' => ['id', 'locale']])
        ->addColumn('id', 'integer', ['null' => false])
        ->addColumn('locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
        ->addColumn('title', 'string', ['null' => true, 'default' => null])
        ->create();
    }
  }
}
