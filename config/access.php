<?php

$config ['Access'] = [
  'languages' => [
    'name' => 'Idiomas',
    'options' => [
      'edit' => [
          'name' => 'Edición',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'I18n',
              'controller' => 'Languages',
              'action' => 'index',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'I18n',
              'controller' => 'Languages',
              'action' => 'create',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'I18n',
              'controller' => 'Languages',
              'action' => 'update',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'I18n',
              'controller' => 'Languages',
              'action' => 'delete',
            ]
          ]
      ]
    ]
  ]
];