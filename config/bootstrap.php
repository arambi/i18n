<?php 
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;
use Cake\Core\Configure;

if( !Configure::read( 'I18n.behavior'))
{
  Configure::write( 'I18n.behavior', 'I18n.I18nable');
}


Access::add( 'languages', [
  'name' => 'Idiomas',
  'options' => [
    'edit' => [
        'name' => 'Edición',
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'Languages',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'Languages',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'Languages',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'Languages',
            'action' => 'delete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'Languages',
            'action' => 'sortable',
          ]
        ]
    ]
  ]
]);



Access::add( 'translations', [
  'name' => 'Traduccion',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'I18n',
          'controller' => 'Translations',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'I18n',
          'controller' => 'Translations',
          'action' => 'excel',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'I18n',
          'controller' => 'Translations',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'I18n',
          'controller' => 'Translations',
          'action' => 'translate',
        ],
      ]
    ]
  ]
]);

if( Configure::read( 'I18n.hasCountries'))
{
  Access::add( 'webcountries', [
    'name' => 'Paises del web',
    'options' => [
      'edit' => [
        'name' => 'Edición',
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'add',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'delete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'sortable',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'I18n',
            'controller' => 'WebCountries',
            'action' => 'field',
          ]
        ]
      ]
    ]
  ]);
  
  NavigationCollection::add( [
    'name' => 'Países del web',
    'parentName' => 'Países del web',
    'plugin' => 'I18n',
    'controller' => 'WebCountries',
    'action' => 'index',
    'icon' => 'fa fa-square',
  ]);
}