<?php
namespace I18n\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use I18n\Controller\AppController;
use Sepia\FileHandler;
use Sepia\poParser;

/**
 * Dictionaries Controller
 *
 * @property \I18n\Model\Table\DictionariesTable $Dictionaries
 */
class DictionariesController extends AppController
{
  use CrudControllerTrait;

  public function index()
  {
    $fileHandler = new FileHandler( APP . 'Locale/eu/default.po');
    $poParser = new poParser( $fileHandler);
    $contents  = $poParser->parse();
    $this->set( compact( 'contents'));
  }
}
