<?php
namespace I18n\Controller\Admin;

use I18n\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Languages Controller
 *
 * @property I18n\Model\Table\LanguagesTable $Languages
 */
class LanguagesController extends AppController 
{
	use CrudControllerTrait;
}
