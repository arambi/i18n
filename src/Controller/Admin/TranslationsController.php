<?php
namespace I18n\Controller\Admin;

use Cake\Core\Plugin;
use Cake\Event\Event;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Cofree\Lib\PluginUtils;
use I18n\Lib\TranslatorCsv;
use I18n\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Translations Controller
 *
 * @property \I18n\Model\Table\TranslationsTable $Translations
 *
 * @method \I18n\Model\Entity\Translation[] paginate($object = null, array $settings = [])
 */
class TranslationsController extends AppController
{
  use CrudControllerTrait {
    initialize as initializeCrud;
  }

  private $tables;

  public function initialize()
  {
    // $model = $this->getRequest()->getQuery( 'model');
    // $this->getTables();
    // $this->modelClass = $model;
    $this->initializeCrud();
  }

  public function excel()
  {
    $file = TMP . rand(0, 99999) .'_'. time() . '.csv';
    $output = fopen($file, 'w');

    $data = (new TranslatorCsv())->run()->getData();

    foreach( $data as $row) {
      fputcsv($output, $row, "\t");
    }

    fclose($output);
    $this->setResponse( $this->response->withFile( $file));
    $this->setResponse( $this->response->withType('application/vnd.ms-excel'));
    return $this->response->withDownload('translations_' . Website::get('title') . '_' . date('d-m-Y') . '.xls');
  }

  public function beforeFilter( Event $event)
  {
    parent::beforeFilter( $event);
    $model = $this->getRequest()->getQuery( 'model');
    $fields = $this->tables [$model];
    
    $this->Table->crud
      ->addIndex( 'index', [
        'template' => 'I18n/translations_index',
        'fields' => $fields
      ])
      ->addView( 'translate', [
        'template' => 'Manager/update',
        'title' => __d( 'admin', 'Editar campo'),
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                ]
              ]
            ],
          ],
        ],
        'saveButton' => true
      ])
      ;
  }

  protected function _index( $query)
  {
    $query->find( 'translations');
  }

  public function translate()
  {
  }

  private function getTables()
  {
    $tables = [];
    $plugins = Plugin::loaded();

    foreach( $plugins as $plugin)
    {
      $models = PluginUtils::getModels( $plugin);

      foreach( $models as $model)
      {
        $table = TableRegistry::getTableLocator()->get( $plugin.'.'. $model);
        $pluginModel = $plugin.'.'. $model;

        if( $table->hasBehavior( 'I18nTranslate'))
        {
          $fields = $table->translateFields();

          if( isset( $tables [$pluginModel]))
          {
            $tables [$pluginModel] = $tables [$pluginModel] + $fields;
            $tables [$pluginModel] = array_unique( $tables [$pluginModel]);
          }
          else
          {
            $tables [$pluginModel] = $fields;
          }
        }
      }
    }

    $this->tables = $tables;
  }

  

}
