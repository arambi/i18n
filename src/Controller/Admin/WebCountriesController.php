<?php
namespace I18n\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use I18n\Controller\AppController;

/**
 * WebCountries Controller
 *
 * @property \I18n\Model\Table\WebCountriesTable $WebCountries
 */
class WebCountriesController extends AppController
{
    use CrudControllerTrait;
}
