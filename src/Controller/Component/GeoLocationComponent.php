<?php

namespace I18n\Controller\Component;

use InvalidArgumentException;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Utility\Inflector;

/**
 * GeoLocation component
 */
class GeoLocationComponent extends Component
{
    const KEY = 'GeoLocation';
    protected $_defaultConfig = [];

    private $_properties;
    private $mockIP = '83.173.153.127';


    private function getLocation()
    {
        $request = $this->getController()->getRequest();
        $session = $request->getSession();

        if ($session->check(self::KEY)) {
            return $session->read(self::KEY);
        }

        $ip = $request->clientIp();
        if (in_array($ip, ['192.168.65.1', '172.28.128.3', '172.18.0.1', '172.23.0.1', '172.28.128.1', '172.28.128.5', '127.0.0.1', '55.55.55.1']) || $_SERVER['SCRIPT_NAME'] == '/usr/local/bin/phpunit') {
            $ip = $this->getMockIP();
        }

        $location = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip), true);

        if (empty($location)) {
            $_location = @json_decode(file_get_contents('https://api.iplocation.net/?ip='. $ip), true);
    
            $mapIpLocation = [
                'geoplugin_countryName' => 'country_name',
                'geoplugin_countryCode' => 'country_code2',
            ];
    
            $location = [];
    
            foreach ($mapIpLocation as $key1 => $key2) {
                if (isset($_location[$key2])) {
                    $location[$key1] = $_location[$key2];
                }
            }
        }

        if (!empty($location) && $location['geoplugin_countryName'] != null) {
            $session->write(self::KEY, $location);
            return $location;
        } else {
            $session->write(self::KEY, false);
        }
    }

    private function getMockIP()
    {
        if (Configure::read('GeoLocation.mockIP')) {
            return Configure::read('GeoLocation.mockIP');
        }

        return $this->mockIP;
    }

    private function setLocation()
    {
        $location = $this->getLocation();

        if (!$location) {
            $this->_properties = [];
            return;
        }

        foreach ($location as $key => $value) {
            $this->_properties[str_replace('geoplugin_', '', $key)] = $value;
        }
    }

    public function get($property)
    {
        if ($this->_properties == null) {
            $this->setLocation();
        }

        if (!strlen((string)$property)) {
            throw new InvalidArgumentException('Cannot get an empty property');
        }

        $value = null;

        if (isset($this->_properties[$property])) {
            $value = &$this->_properties[$property];
        }

        $method = Inflector::camelize($property);

        if (method_exists($this, '_get' . $method)) {
            $result = $this->{$method}($value);
            return $result;
        }

        return $value;
    }
}
