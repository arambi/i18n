<?php
namespace I18n\Controller;

use I18n\Controller\AppController;

/**
 * Dictionaries Controller
 *
 * @property \I18n\Model\Table\DictionariesTable $Dictionaries
 */
class DictionariesController extends AppController
{
}
