<?php
namespace I18n\Controller;

use I18n\Controller\AppController;

/**
 * WebCountries Controller
 *
 * @property \I18n\Model\Table\WebCountriesTable $WebCountries
 */
class WebCountriesController extends AppController
{
  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this->WebCountries
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this->WebCountries
      ->find( 'front')
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
