<?php

namespace I18n\I18n;

use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use I18n\Lib\Lang;


class Translator
{
  public static $nodes = array();
  
  public static $loaded = false;

  public static function build()
  {
    static::$nodes = TableRegistry::get( 'I18n.Dictionaries')->getAll();
    static::$loaded = true;
  }
  
  public static function get( $text, $domain = 'default')
  {
    if( !self::$loaded)
    {
      static::build();
    }
    else
    {

    }
    
    $lang = Lang::current( 'iso3');

    $node = static::getNode( $text);
    
    if( $result = Hash::get( static::$nodes, $domain .'.'.  $node . '.'. $lang .'.text'))
    {
      return $result;
    }

    return $text;
  }
  
  public static function getNode( $text)
  {
    return md5( $text);
  }
}