<?php

namespace I18n\Lib;

use Cake\Core\Plugin;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cofree\Lib\PluginUtils;
use Google\Cloud\Translate\V2\TranslateClient;

class GoogleTranslator
{
    private $langs;

    private $tables;

    private $mainLang = 'spa';

    private $currentLang;

    private $charsTotal = 0;

    private $translator;

    public function __construct()
    {
        $this->setTables();
        $this->setTranslator();
    }

    private function setTranslator()
    {
        $this->translator = new TranslateClient([
            'key' => Configure::read('Google.translator.apikey')
        ]);
    }

    private function setTables()
    {
        $this->tables = $this->getTables();
    }

    public function setMainLang($lang)
    {
        $this->mainLang = $lang;
        return $this;
    }

    public function run()
    {
        $langs = collection(Lang::get())->extract('iso3')->toArray();

        foreach ($langs as $lang) {
            if ($lang == $this->mainLang) {
                continue;
            }

            $this->langs = [$this->mainLang, $lang];
            $this->currentLang = $lang;

            foreach ($this->tables as $model => $fields) {
                $table = TableRegistry::getTableLocator()->get($model);
                $query = $table->find('translations');
                $this->set($query, $table, $fields);
            }
        }

        return $this;
    }

    public function set($query, $table, $keys)
    {
        $limit = 50;
        $offset = 0;
        $fields = [];

        foreach ($keys as $key) {
            $field = $table->crud->fieldCollector()->get($key);
            $fields[$field->name()] = $field->value('label');
        }

        while (true) {
            $_query = clone $query;
            $_query
                ->limit($limit)
                ->offset($offset)
                ->order($table->getAlias() . '.' . $table->getPrimaryKey());

            $contents = $_query->all();

            foreach ($contents as $content) {
                $this->translateFields($content, $fields, $table);
            }

            $offset += $limit;

            if ($contents->count() < $limit) {
                break;
            }
        }
    }

    public function translateFields($content, $fields, $table)
    {
        foreach (array_keys($fields) as $field) {
            foreach ($this->langs as $lang) {
                if ($lang == $this->mainLang) {
                    continue;
                }

                $value = $content->translation($lang)->get($field);
                $original = $content->translation($this->mainLang)->get($field);

                if (!empty($original) && empty($value)) {

                    $this->charsTotal += strlen($original);
                    $text = $this->translate($this->mainLang, $lang, $original);

                    if (!empty($text)) {
                        $this->saveTranslate($text, $content, $field, $lang, $table);
                    }
                }
            }
        }
    }

    public function saveTranslate($text, $content, $field, $locale, $table)
    {
        $entity = $table->translationTable()->find()
            ->where([
                'id' => $content->id,
                'locale' => $locale,
            ])
            ->first();

        if (!$entity) {
            $entity = $table->translationTable()->newEntity([
                'id' => $content->id,
                'locale' => $locale,
            ]);

            $entity = $table->translationTable()->save($entity);
        }

        $table->translationTable()->query()->update()
            ->set([
                $field => $text
            ])
            ->where([
                'id' => $content->id,
                'locale' => $locale,

            ])
            ->execute();
    }

    public function translate($source, $target, $text)
    {
        if ($target == 'eus') {
            $target = 'baq';
        }
        
        $result = $this->translator->translate($text, [
            'source' => $source,
            'target' => $target,
        ]);


        print("$text \n");

        if (isset($result['text'])) {
            print("{$result['text']} \n");
            return $result['text'];
        }
    }


    public function getCharsTotal()
    {
        return $this->charsTotal;
    }

    private function getTables()
    {
        $tables = [];
        $plugins = Plugin::loaded();

        foreach ($plugins as $plugin) {
            $models = PluginUtils::getModels($plugin);

            foreach ($models as $model) {
                $table = TableRegistry::getTableLocator()->get($plugin . '.' . $model);
                $pluginModel = $plugin . '.' . $model;

                if ($table->hasBehavior('I18nTranslate')) {
                    $fields = $table->translateFields();

                    if (isset($tables[$pluginModel])) {
                        $tables[$pluginModel] = $tables[$pluginModel] + $fields;
                        $tables[$pluginModel] = array_unique($tables[$pluginModel]);
                    } else {
                        $tables[$pluginModel] = $fields;
                    }
                }
            }
        }

        return $tables;
    }
}
