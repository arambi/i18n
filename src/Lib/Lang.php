<?php
namespace I18n\Lib;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\I18n\I18n;
use \Locale;
use Cake\Collection\Collection;

class Lang
{
  protected static $_languages = [];

  protected static $_keys = [];

  protected static $_iso3 = [];

  protected static $_default = false;

  protected static $_loaded = false;

  protected static $_all = [];

  protected static $_current = false;

  public static function load( $force = false)
  {
    if( !self::$_loaded)
    {
      // Comprueba primero que exista la tabla languages
      $tables = TableRegistry::get( 'I18n.Languages')->getConnection()->query( 'SHOW tables')->fetchAll();
      $tables = array_map( function( $value){ 
          return $value [0];
      }, $tables);

      if( !in_array( 'languages', $tables))
      {
        return;
      }

      $Languages = TableRegistry::get( 'I18n.Languages');
      $result = $Languages->find()->all();
      self::set( $result);

      self::getCurrent( true);
    }
  }

  public static function reload()
  {
    self::load( true);
  }

  public static function getCurrent( $actives = false)
  {
    if( $actives)
    {
      $keys = self::$_all->match(['published' => true])->combine( 'iso2', 'name')->toArray();
    }
    else
    {
      $keys = array_keys( self::keys());
    }

    $url = env( 'REQUEST_URI');
    $params = explode( '/', $url);
    $keys = array_keys( self::keys());

    if( isset( $params [1]) && in_array( self::getLocaleFromString($params [1]), $keys))
    {
      I18n::setLocale( self::getIso3( self::getLocaleFromString($params [1])));
    }
    else
    {
      if( Configure::read( 'I18n.forceDefault'))
      {
        $language = self::byDefault();
        
        if( $language)
        {
          I18n::setLocale( self::getIso3( $language->iso2));
        }
        return;
      }

      if( Configure::read( 'I18n.domains'))
      {
        foreach( Configure::read( 'I18n.domains') as $locale => $domain)
        {
          if( strpos( $domain, @$_SERVER ['SERVER_NAME']) !== false)
          {
            I18n::setLocale( self::getIso3( $locale));
            return;
          }
        }
      }

      if( isset( $_SERVER ['HTTP_ACCEPT_LANGUAGE']))
      {
        $locale = Locale::acceptFromHttp( $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $primary = Locale::getPrimaryLanguage( $locale);
      }
        

      if( isset( $_SERVER ['HTTP_ACCEPT_LANGUAGE']) && self::$_all->match(['published' => true])->firstMatch( ['iso2' => $primary]))
      {
        I18n::setLocale( self::getIso3( $primary));
        $locale = self::current('locale');
        setlocale( LC_ALL, $locale .'.UTF-8');
      }
      else
      {
        $language = self::byDefault();

        if( $language)
        {
          I18n::setLocale( self::getIso3( $language->iso2));
        }
      }        
    }
  }

  public static function getLocaleFromString( $param)
  {
    if( strlen( $param) == 5 && strpos( $param, '-') !== false)
    {
      list( $country, $lang) = explode( '-', $param);
      return $lang;
    }

    return $param;
  }

  public static function current( $key = 'iso2')
  {
    self::load();
    return self::$_all->firstMatch( ['iso3' => I18n::getLocale()])->$key;
  }

  public static function set( $result)
  {
    self::$_all = $result;
    $languages = $result->toArray();

    self::$_languages = $result->toArray();
    self::$_keys = $result->combine( 'iso2', 'name')->toArray();
    self::$_iso3 = $result->combine( 'iso3', 'name')->toArray();
    self::$_default = $result->firstMatch( ['by_default' => 1]);
    self::$_loaded = true;
  }

  public static function get( $published = false)
  {
    $langs = self::$_languages;;

    if( $published)
    {
      $langs = array_filter( $langs, function( $value){
        return $value->published;
      });
    }

    return $langs;
  }

  public static function keys( $actives = false)
  {
    if( empty( self::$_all) || self::$_all->count() == 0)
    {
      return [];
    }

    if( $actives)
    {
      return self::$_all->match(['published' => true])->combine( 'iso2', 'name')->toArray();
    }
    
    return self::$_keys;
  }

  public static function iso3()
  {
    return self::$_iso3;
  }
  
  public static function combine( $key, $value)
  {
    $collection = new Collection( self::$_languages);
    return $collection->combine( $key, $value)->toArray();
  }

  public static function getIso2( $iso3)
  { 
    return self::$_all->firstMatch( ['iso3' => $iso3])->iso2;
  }

  public static function getIso3( $iso2)
  { 
    return self::$_all->firstMatch( ['iso2' => $iso2])->iso3;
  }

  public static function getName( $iso3)
  { 
    $language = self::$_all->firstMatch( ['iso3' => $iso3]);

    if( !is_object( $language))
    {
      return null;
    }
    
    return self::$_all->firstMatch( ['iso3' => $iso3])->name;
  }

  public static function byDefault()
  {
    return self::$_default;
  }
}