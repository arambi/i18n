<?php 

namespace I18n\Lib;

use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class Translator
{
  
  private $filePath;

  private $file;
  
  private $doc;

  private $section;

  private $models = [];

  private $primaryLang = 'spa';

  public function __construct( $models)
  {
    $this->models = $models;
    $this->filePath = TMP;
  }

  public function write()
  {
    $langs = array_keys( Lang::iso3());

    foreach( $langs as $lang)
    {
      if( $lang != $this->primaryLang)
      {
        $this->writeLang( $lang);
      }
    }
  }

  private function writeLang( $lang)
  {
    $this->doc = new \PhpOffice\PhpWord\PhpWord();
    $this->section = $this->doc->addSection();;

    foreach( $this->models as $model)
    {
      $this->writeModel( $model, $lang);
    }

    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->doc, 'HTML');
    $objWriter->save( $this->filePath .'translates_'. $lang .'.docx');
  }

  private function append( $row)
  {
    $this->section->addText( '<br>');
    $this->section->addText( $row ['metadata'], ['name' => 'Arial', 'size' => 8]);
    $this->section->addText( $row ['original'], ['color' => '999999']);
    $this->section->addText( $row ['translation']);
  }

  private function writeModel( $model, $lang)
  {
    $limit = 400;
    $offset = 0;
    $table = TableRegistry::get( $model);
    $translationTableName = $table->table() . '_translations';
    $translationTable = TableRegistry::get( $translationTableName);
    $fields = $table->translateFields();
    $rows = [];

    while( true)
    {
      $query = $translationTable->find()
        ->where([
          'locale' => $lang
        ])
        ->limit( $limit)
        ->offset( $offset)
        ->order([
          'id'
        ])
      ;

      $contents = $query->all();

      if( $contents->count() == 0)
      {
        break;
      }

      foreach( $contents as $content)
      {
        $original = $this->getPrimaryRegistry( $translationTable, $content->id);
        $post = $table->find()->where([$table->alias() . '.id' => $content->id])->first();
        if( !$post)
        {
          continue;
        }
        
        foreach( $fields as $field)
        {
          if( !empty( $original->$field))
          {
            $metadata = [
              $translationTableName,
              $content->id,
              $lang,
              $table->crud->getName()['singular'],
              !in_array( $table->alias(), ['Letters', 'Fields', 'Options']) && $post && method_exists( $post, 'link') ? Router::url( $post->link(), true) : ''
            ];

            $row = [
              'metadata' => implode( ' | ', $metadata),
              'original' => $original->$field,
              'translation' => $content->$field
            ];

            $this->append( $row);
          }

        }
      }

      $offset = $offset + $limit;
    }
  }
  
  private function getPrimaryRegistry( $table, $id)
  {
    $content = $table->find()
      ->where([
        'locale' => $this->primaryLang,
        'id' => $id
      ])
      ->first();
    
    return $content;
  }
}