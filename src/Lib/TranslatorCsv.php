<?php

namespace I18n\Lib;

use Cake\Core\Plugin;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cofree\Lib\PluginUtils;
use PhpOffice\PhpWord\PhpWord;

/**
 * @property \PhpOffice\PhpWord\PhpWord word
 * @property \PhpOffice\PhpWord\Element\Section section
 * @property \PhpOffice\PhpWord\Writer\Word2007\Element\Table table
 */

class TranslatorCsv
{
    private $langs;

    private $word;

    private $section;

    private $table;

    private $tables;

    private $borderStyle;

    private $mainLang = 'spa';

    private $currentLang;

    private $files = [];

    public function __construct()
    {
        \PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);
        $this->setTables();
        $this->borderStyle = [
            'borderColor' => '#000000',
            'borderSize' => $this->points(.1),
        ];
    }

    private function setTables()
    {
        $this->tables = $this->getTables();
    }

    private function setMainLang($lang)
    {
        $this->mainLang = $lang;
        return $this;
    }

    public function run()
    {
        $langs = collection(Lang::get())->extract('iso3')->toArray();

        foreach ($langs as $lang)
        {
            if ($lang == $this->mainLang) {
                continue;
            }

            $this->langs = [$this->mainLang, $lang];
            $this->currentLang = $lang;

            $this->word = new PhpWord();
            $this->section = $this->word->addSection();
            $this->table = $this->section->addTable([
                'cellMargin' => $this->mm(2)
            ]);

            foreach ($this->tables as $model => $fields) {
                try {
                    $table = TableRegistry::getTableLocator()->get($model);
                    $query = $table->find('translations');
                    $this->set($query, $table, $fields);
                    $this->write();
                } catch (\Throwable $th) {
                    throw $th;
                }
            }

        }
    }

    public function write()
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->word, 'Word2007');
        $objWriter->save(TMP . '/translations_'. $this->currentLang . '.docx');
    }

    public function set($query, $table, $keys)
    {
        $limit = 50;
        $offset = 0;
        $fields = [];

        foreach ($keys as $key) {
            $field = $table->crud->fieldCollector()->get($key);
            $fields[$field->name()] = $field->value('label');
        }

        while (true) {
            $_query = clone $query;
            $_query
                ->limit($limit)
                ->offset($offset)
                ->order($table->getAlias() . '.' . $table->getPrimaryKey());

            $contents = $_query->all();

            foreach ($contents as $content) {
                $this->writeRows($content, $fields);
            }

            $offset += $limit;

            if ($contents->count() < $limit) {
                break;
            }
        }
    }

    public function writeRows($content, $fields)
    {
        foreach (array_keys($fields) as $field) {
            $empty = true;
            $row = [];

            foreach ($this->langs as $lang) {
                $value = $content->translation($lang)->get($field);
                $row[] = $value;

                if (!empty($value)) {
                    $empty = false;
                }
            }

            if (!$empty) {
                $this->addLinks($content);
                $this->addRow();

                foreach ($row as $text) {
                    if (!empty($text)) {
                        if ($this->isHtml($text)) {
                            $this->addCellHtml($this->sanitizeHtml($text));
                        } else {
                            $this->addCell($this->sanitizeText($text));
                        }
                    }
                }
            }
        }
    }

    private function addLinks($content)
    {
        $link = $this->contentLink($content);

        if ($link) {
            $text = "<p><strong>Contexto</strong>: <a href=\"$link\">$link</a></p>";
        } else {
            $text = '';
        }

        $this->addRow();
        $this->addCellHtml($text, [
            'bgColor' => '#e7e7e7',
            'gridSpan' => 2
        ]);
    }

    private function contentLink($content)
    {
        try {
            return Router::url($content->link(), true);
        } catch (\Throwable $th) {
            return '';
        }
    }

    private function addRow()
    {
        $this->table->addRow('', $this->borderStyle);
    }

    private function addCell($text, $styles = [])
    {
        $this->table->addCell($this->mm(80), $this->borderStyle + $styles)->addText($text);
    }

    private function addCellHtml($text, $styles = [])
    {
        try {
            $cell = $this->table->addCell($this->mm(80), $this->borderStyle + $styles);
            \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $text, false, false);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    private function getTables()
    {
        $tables = [];
        $plugins = Plugin::loaded();

        foreach ($plugins as $plugin) {
            $models = PluginUtils::getModels($plugin);

            foreach ($models as $model) {
                $table = TableRegistry::getTableLocator()->get($plugin . '.' . $model);
                $pluginModel = $plugin . '.' . $model;

                if ($table->hasBehavior('I18nTranslate')) {
                    $fields = $table->translateFields();

                    if (isset($tables[$pluginModel])) {
                        $tables[$pluginModel] = $tables[$pluginModel] + $fields;
                        $tables[$pluginModel] = array_unique($tables[$pluginModel]);
                    } else {
                        $tables[$pluginModel] = $fields;
                    }
                }
            }
        }

        return $tables;
    }

    private function isHtml($string)
    {
        return preg_match("/<[^<]+>/", $string, $m) != 0;
    }

    private function sanitizeHtml($html)
    {
        $html = preg_replace("/<img[^>]+\>/i", "(image) ", $html);
        $html = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $html);
        $html = preg_replace('/(<[^>]+) align=".*?"/i', '$1', $html);
        $html = html_entity_decode($html);
        return $this->encode($html);
    }

    private function sanitizeText($html)
    {
        return $this->encode($html);
    }

    private function encode($text)
    {
        return strip_tags($text, '<p><br><strong><em><i><b>');
    }

    private function mm($twips)
    {
        return $twips  * 56.692913386;
    }

    private function points($twips)
    {
        return $twips  * 20;
    }
}
