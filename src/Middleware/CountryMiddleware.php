<?php 

namespace I18n\Middleware;

use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\Http\Cookie\Cookie;

class CountryMiddleware
{
  public function __invoke($request, $response, $next)
  {   
    $uri = $request->getUri();
    $parts = explode( '/', $uri->getPath());

    if( strlen( $parts [1]) == 5 && strpos( $parts [1], '-') !== false)
    {
      list( $country, $lang) = explode( '-', $parts [1]);
      Configure::write( 'Country', $country);
      $request = $request->withAttribute( 'country', $country);
      $parts [1] = $lang;
      $uri = $uri->withPath( implode( '/', $parts));
      $request = $request->withUri( $uri);
    }
    elseif( Configure::read( 'Country'))
    {
      $request->withAttribute( 'country', Configure::read( 'Country'));
    }

    $response = $next($request, $response);
    return $response;
  }
}