<?php
namespace I18n\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use ShadowTranslate\Model\Behavior\ShadowTranslateBehavior;
use Cake\Utility\Inflector;
use Cake\ORM\Query;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Core\Configure;
use I18n\Lib\Lang;
use ArrayObject;

/**
 * I18nTranslate behavior
 */
class I18nTranslateBehavior extends ShadowTranslateBehavior
{

  public function __construct( Table $table, array $config = [])
  {
    $config ['implementedMethods']['translateFields'] = 'translateFields';
    $config ['implementedMethods']['hasTranslate'] = 'hasTranslate';
    $config ['implementedMethods']['translationTable'] = 'translationTable';
    $config ['implementedMethods']['addFieldTranslate'] = 'addField';
    $config ['implementedFinders']['localeActives'] = 'findLocaleActives';
    $config ['implementedFinders']['empty'] = 'findEmpty';

    if( array_key_exists( 'translationTable', $config))
    {
      unset( $config ['translationTable']);
    }

    $config += [
        'translationTable' => Inflector::camelize( $table->table() . '_translations'),
    ];

    parent::__construct( $table, $config);
  }

  
  public function addField( $field)
  {
    $fields = $this->getConfig( 'fields');
    $fields [] = $field;
    $this->setConfig( 'fields', $fields);
  }

/**
 * Evita que se hagan queries con el clause `where` con solamente `id`, es decir sin alias de la table
 * Lo que hace es insertar el alias para construir queries completas tipo `alias.id`
 * Evita lo que parece un bug de CakePHP
 * 
 * @param  Event  $event   
 * @param  Query  $query   
 * @param  [type] $options 
 * @return
 */
  public function beforeFind(Event $event, Query $query, $options)
  {
    parent::beforeFind( $event, $query, $options);
    $query->find( 'empty');

    $where = $query->clause( 'where');

    if( $where instanceof \Cake\Database\Expression\QueryExpression)
    {
      $query->clause( 'where')->iterateParts( 
        function( $part)
        {
          if( $part instanceof \Cake\Database\Expression\Comparison)
          {
            $field = $part->getField();

            if( $field == $this->_table->primaryKey())
            {
              $part->setField( $this->_table->alias() .'.'. $field);
            }

            $this->__setTranslateField( $part);
          }
          elseif( $part instanceof \Cake\Database\Expression\QueryExpression)
          {
            $this->__iterateParts( $part);
          }

          return $part;
        }
      );
    }

    $order = $query->clause( 'order');

    if( $order !== null)
    {
      $this->_order = [];

      $order->iterateParts( function( $direction, $field){
          $posible = false;

          if( is_numeric( $field))
          {
            $field = $direction;
            $direction = 'asc';
          }

          if( strpos( $field, '.') !== false)
          {
            list( $_table, $_field) = explode( '.', $field);

            if( $_table == $this->_table->alias())
            {
              $posible = $_field;
            }
          }
          else
          {
            $posible = $field;
          }

          if( $posible && $this->hasTranslate( $posible))
          {
            $alias = $this->config( 'hasOneAlias');
            $field = $alias .'.'. $posible;
          }

          $this->_order [$field] = $direction;
        }
      );

      $order->add( $this->_order);
    }
  }

/**
 * Rellena los campos vacios de uno idioma con los del idioma principal
 *
 * @param Query $query
 * @return void
 */
  public function findEmpty( Query $query)
  {
    if( !Configure::read( 'I18n.fillReference'))
    {
      $keys = array_keys( Lang::iso3());
  
      foreach( $keys as $i => $key)
      {
        if( $key == Lang::current( 'iso3'))
        {
          unset( $keys [$i]);
        }
      }
    }
    else
    {
      $keys = [Configure::read( 'I18n.fillReference')];
    }

    return $query->formatResults( function( $results) use( $keys){
      return $results->map( function( $row) use( $keys){
        if( !is_object( $row) || !$this->fillFields( $row))
        {
          return $row;
        }
        
        $entities = false;

        foreach( $this->getConfig( 'fields') as $field)
        {
          if( empty( $row->get( $field)))
          {
            if( !$entities)
            {
              $entities = $this->_translationTable()->find()
                ->where([
                  'id' => $row->get( $this->_table->getPrimaryKey()),
                  'locale IN' => $keys
                ])
                ->toArray();
            }

            if( !empty( $entities))
            {
              foreach( $entities as $entity)
              {
                if( !empty( $entity->get( $field)))
                {
                  $row->set( $field, $entity->get( $field));
                }
              }
            }
          }
        }

        return $row;
      });
    });
  }


  private function fillFields( $entity)
  {
    $model = $entity->getSource();
    $models = Configure::read( 'I18n.fillModels');

    if( !$models)
    {
      return false;
    }

    return in_array( $model, $models);
  }

  /**
 * En caso de que el campo usado en where() sea uno de traducción, se setea el campo de la relación hasOne
 * Si fuera Contents.title se setea ese campo como Contents_title_translation.content
 * De esta manera la búsqueda se hace sobre el campo de traducción
 * 
 * @param  Comparison $part
 */
  private function __setTranslateField( $part)
  {
    if( count( Lang::keys()) < 1)
    {
      return;
    }

    $field = $part->getField();

    if( is_string($field) && strpos( $field, '.') !== false)
    {
      list( , $field) = explode( '.', $field);
    }

    if( $this->hasTranslate( $field))
    {
      $alias = $this->config( 'hasOneAlias');
      $new = $alias .'.'. $field;
      $part->setField( $new);
    }
  }


  private function __iterateParts( $part)
  {
    $part->iterateParts(
      function( $part)
      {
        
        if( $part instanceof \Cake\Database\Expression\Comparison)
        {
          $field = $part->getField();

          if( $field == $this->_table->primaryKey())
          {
            $part->setField( $this->_table->alias() .'.'. $field);
          }

          $this->__setTranslateField( $part);
        }
        if( $part instanceof \Cake\Database\Expression\QueryExpression)
        {
          $this->__iterateParts( $part);
        }
        return $part;
      }
    );
  }

  public function translateFields()
  {
    return $this->config( 'fields');
  }

  public function hasTranslate( $key)
  {
    $fields = $this->translateFields();
    return in_array( $key, $fields);
  }

  public function findLocaleActives( Query $query)
  {
    if( !$this->_table->isAdmin())
    {
      $displayField = $this->_table->displayField();

      if( $this->hasTranslate( $displayField))
      {
        $query->where([
          $this->_table->alias() . '.'. $displayField .' !=' => ''
        ]);
      }
    }

    return $query;
  }

  public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
  {
      $locale = $entity->get('_locale') ?: $this->locale();
      $this->_setEmptyFields( $entity, $locale);
      $newOptions = [$this->_translationTable->alias() => ['validate' => false]];
      $options['associated'] = $newOptions + $options['associated'];

      $this->_bundleTranslatedFields($entity);
      $bundled = $entity->get('_i18n') ?: [];
      $values = $entity->extract($this->_translationFields(), true);
      $fields = array_keys($values);

      if (empty($fields)) {
          return;
      }
      $primaryKey = (array)$this->_table->primaryKey();
      $id = $entity->get(current($primaryKey));
      $where = compact('id', 'locale');

      $translation = $this->_translationTable()->find()
          ->select(array_merge(['id', 'locale'], $fields))
          ->where($where)
          ->bufferResults(false)
          ->first();


      if (!$translation) {
          $translation = [];
      } 

      $entity->set('_i18n', array_merge($bundled, [$translation]));
      $entity->set('_locale', $locale, ['setter' => false]);
      $entity->dirty('_locale', false);

      foreach ($fields as $field) {
          $entity->dirty($field, false);
      }
  }

  /**
     * Modifies the results from a table find in order to merge full translation records
     * into each entity under the `_translations` key
     *
     * @param \Cake\Datasource\ResultSetInterface $results Results to modify.
     * @return \Cake\Collection\Collection
     */
    public function groupTranslations($results)
    {
        return $results->map(function ($row) {
            if( $row === null)
            {
              return $row;
            }

            $translations = (array)$row['_i18n'];
            if (count($translations) === 0 && $row->get('_translations')) {
                return $row;
            }

            $result = [];
            foreach ($translations as $translation) {
                unset($translation['id']);
                $result[$translation['locale']] = $translation;
            }

            $row['_translations'] = $result;
            unset($row['_i18n']);
            if (is_object($row)) {
                $row->clean();
            }

            return $row;
        });
    }

    protected function _setEmptyFields( $entity, $locale)
    {
      if( !Configure::read( 'I18n.fillFields'))
      {
        return;
      }

      if( !in_array( $entity->getSource(), (array)Configure::read( 'I18n.fillFields.models')))
      {
        return;
      }

      $exceptions = (array)Configure::read( 'I18n.fillFields.localeExceptions');

      $fields = $this->translateFields();

      foreach( $fields as $field)
      {
        $value = $entity->translation( $locale)->get( $field);
        
        if( !empty( $value))
        {
          foreach( $entity->_translations as $_locale => $values)
          {
            if( !in_array( $_locale, $exceptions) && $locale != $_locale && empty( $entity->translation( $_locale)->get( $field)))
            {
              $entity->translation( $_locale)->set( $field, $value);
            }
          }
        }
      }

    }

  public function translationTable()
  {
    return $this->_translationTable();
  }
}
