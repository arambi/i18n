<?php
namespace I18n\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\I18n\I18n;
use Cake\Event\Event;
use Cake\ORM\Query;
use I18n\Lib\Lang;

/**
 * I18nable behavior
 */
class I18nableBehavior extends Behavior 
{

/**
 * Default configuration.
 *
 * @var array
 */
	protected $_defaultConfig = [
    'implementedMethods' => [
      'translateFields' => 'translateFields',
      'addTranslateFields' => 'addTranslateFields',
      'hasTranslate' => 'hasTranslate',
      'translateTable' => 'translateTable'
    ],
    'implementedFinders' => [
      'localeActives' => 'findLocaleActives',
    ]
  ];

  public function initialize(array $config) 
  {
    $this->_table->addBehavior( 'Translate', $config);
  }

/**
 * Evita que se hagan queries con el clause `where` con solamente `id`, es decir sin alias de la table
 * Lo que hace es insertar el alias para construir queries completas tipo `alias.id`
 * Evita lo que parece un bug de CakePHP
 * 
 * @param  Event  $event   
 * @param  Query  $query   
 * @param  [type] $options 
 * @return
 */
  public function beforeFind(Event $event, Query $query, $options)
  {
    $where = $query->clause( 'where');

    if( $where instanceof \Cake\Database\Expression\QueryExpression)
    {
      $query->clause( 'where')->iterateParts( 
        function( $part)
        {
          if( $part instanceof \Cake\Database\Expression\Comparison)
          {
            $field = $part->getField();
            
            if( $field == $this->_table->primaryKey())
            {
              $part->setField( $this->_table->alias() .'.'. $field);
            }

            $this->__setTranslateField( $part);
          }
          elseif( $part instanceof \Cake\Database\Expression\QueryExpression)
          {
            $this->__iterateParts( $part);
          }

          return $part;
        }
      );
    }
  }

  public function findLocaleActives( Query $query)
  {
    if( !$this->_table->isAdmin())
    {
      $displayField = $this->_table->displayField();

      if( $this->hasTranslate( $displayField))
      {
        $query->where([
          $this->_table->alias() . '.'. $displayField .' !=' => ''
        ]);
      }
    }

    return $query;
  }

/**
 * En caso de que el campo usado en where() sea uno de traducción, se setea el campo de la relación hasOne
 * Si fuera Contents.title se setea ese campo como Contents_title_translation.content
 * De esta manera la búsqueda se hace sobre el campo de traducción
 * 
 * @param  Comparison $part
 */
  private function __setTranslateField( $part)
  {
    if( count( Lang::keys()) < 1)
    {
      return;
    }

    $field = $part->getField();

    if( strpos( $field, '.') !== false)
    {
      list( , $field) = explode( '.', $field);
    }

    if( $this->hasTranslate( $field))
    {
      $new = $this->_table->alias() .'_'. $field .'_translation.content';
      $part->setField( $new);
    }
  }

  private function __iterateParts( $part)
  {
    $part->iterateParts(
      function( $part)
      {
        
        if( $part instanceof \Cake\Database\Expression\Comparison)
        {
          $field = $part->getField();

          if( $field == $this->_table->primaryKey())
          {
            $part->setField( $this->_table->alias() .'.'. $field);
          }

          $this->__setTranslateField( $part);
        }
        if( $part instanceof \Cake\Database\Expression\QueryExpression)
        {
          $this->__iterateParts( $part);
        }
        return $part;
      }
    );
  }

/**
 * Nos devuelve los campos traducibles
 * 
 * @return array
 */
  public function translateFields()
  {
    return $this->config( 'fields');
  }

  public function hasTranslate( $key)
  {
    $fields = $this->translateFields();
    return in_array( $key, $fields);
  }

/**
 * Añade nuevos campos a la traducción
 * @param array $new
 */
  public function addTranslateFields( $new)
  {
    $this->config( 'fields', $new);
  }

  public function translateTable()
  {
    $table = $this->config( 'translationTable');

    if( $table === null)
    {
      return 'I18n';
    }

    return $table;
  }
}
