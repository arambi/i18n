<?php

namespace I18n\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;
use Cake\Utility\Text;
use Blog\Model\Entity\BlogEntityTrait;
use Website\Lib\Website;

trait DatesEntityTrait
{

    private $SECOND = 0;
    private $MINUTE = 60;
    private $HOUR =  60 * 60;
    private $DAY = 24 * 60 * 60;
    private $MONTH = 30 * 24 * 60 * 60;

    public function datePost($field)
    {
        return strftime(Website::get('date_post'), $this->getUnixString($field));
    }

    public function datePostShort($field)
    {

        return strftime(Website::get('date_post_short'), $this->getUnixString($field));
    }

    public function dateEventPunctual($field)
    {
        return strftime(Website::get('date_event_punctual'), $this->getUnixString($field));
    }

    public function getUnixString($field)
    {
        if (is_object($this->$field)) {
            $time = $this->$field->toUnixString();
        } else {
            $time = strtotime($this->$field);
        }

        return $time;
    }


    /**
     * Humanize by delta.
     *
     * @time the unix timestamp 
     * @return the human time text since time 
     */
    public function timeago($key)
    {
        $field = $this->get($key);

        if (!$field) {
            return;
        }

        $delta = time() - (int)$field->toUnixString();

        if ($delta < 1 * $this->MINUTE) {
            return $delta == 1 ? __d( 'app', 'hace un momento') : __d( 'app', 'hace {0} segundos', [$delta]);
        }
        if ($delta < 2 * $this->MINUTE) {
            return __d( 'app', 'hace un minuto');
        }
        if ($delta < 45 * $this->MINUTE) {
            return __d( 'app', 'hace {0} minutos', [floor($delta / $this->MINUTE)]);
        }
        if ($delta < 90 * $this->MINUTE) {
            return __d( 'app', 'hace una hora');
        }
        if ($delta < 24 * $this->HOUR) {
            return __d( 'app', 'hace {0} horas', [floor($delta / $this->HOUR)]);
        }
        if ($delta < 48 * $this->HOUR) {
            return __d( 'app', 'ayer');
        }
        if ($delta < 30 * $this->DAY) {
            return __d( 'app', 'hace {0} días', [floor($delta / $this->DAY)]);
        }
        if ($delta < 12 * $this->MONTH) {
            $months = floor($delta / $this->DAY / 30);
            return $months <= 1 ? __d( 'app', 'hace un mes') : __d( 'app', 'hace {0} meses', [$months]);
        } else {
            $years = floor($delta / $this->DAY / 365);
            return $years <= 1 ? __d( 'app', 'hace un año') : __d( 'app', 'hace {0} años', [$years]);
        }
    }
}
