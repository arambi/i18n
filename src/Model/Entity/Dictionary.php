<?php
namespace I18n\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;use Cake\ORM\Entity;

/**
 * Dictionary Entity.
 *
 * @property int $id
 * @property string $node
 * @property string $domain
 * @property bool $msgid_plural
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Dictionary extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
