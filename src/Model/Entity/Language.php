<?php
namespace I18n\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Language Entity.
 */
class Language extends Entity 
{

  use CrudEntityTrait;
/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'*' => true,
    'id' => true,
		'name' => true,
		'iso2' => true,
		'iso3' => true,
		'locale' => true,
		'key' => true,
		'by_default' => true,
		'position' => true,
	];

}
