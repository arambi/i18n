<?php
namespace I18n\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use I18n\Model\Entity\Dictionary;
use I18n\Lib\Lang;
use Cake\Cache\Cache;

/**
 * Dictionaries Model
 */
class DictionariesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table(null);
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'I18n.I18nable', [
      'fields' => ['msgstr'],
      'translationTable' => 'I18n.DictionariesI18n',
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'text' => [
          'label' => 'Texto',
          'type' => 'text'
        ],
        'domain' => 'Referencia',
        
      ])
      ->addIndex( 'index', [
        'fields' => [
          'msgstr',
          'domain',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => 'Traducción',
        'plural' => 'Traducción',
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'msgstr',
                  'domain',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
      
  }

/**
 * Añade un nodo 
 *
 * @param string $text 
 * @param string $domain 
 * @param array $references 
 * @param string $msgid_plural 
 * @return void
 */
  public function add( $text, $domain = 'default', $msgid_plural = false)
  {
    $query = $this->find()
      ->where([
        'node' => md5($text)
      ]);

    $record = $query->first();

    if( $record)
    {
      return $record;
    }

    foreach( array_keys( Lang::iso3()) as $locale)
    {
      $data [$locale]['text'] = $text;
    }
    
    $data += [
        'node' => md5( $text),
        'node_text' => $text,
        'domain' => $domain,
        'msgid_plural' => $msgid_plural,
    ];
    
    $entity = $this->getNewEntity( $data);
        
    if( $this->saveContent( $entity))
    {    
      return $entity;
    }
    
    return false;
  }
  
  public function getAll()
  {    
    $results = Cache::read( 'Dictionaries');
    $results = [];
    
    if( !$results)
    {
      $records = $this->find( 'translations')->all()->toArray();
      $records = json_decode( json_encode( $records), true);

      foreach( $records as $record)
      {
        $results [$record ['domain']][$record ['node']] = $record ['_translations'];
      }

      Cache::write( 'Dictionaries', $results);
    }
    
    return $results;
  }

/**
 * Toma un nodo dado un texto y un domain
 *
 * @param string $text 
 * @param string $domain 
 * @return void
 */
  public function get( $text, $domain = 'default')
  {
    $records = $this->getAll();
    $node = $this->getCrypt( $text);
    
    if( isset( $this->all [$domain][$node]))
    {
      return $this->all [$domain][$node];
    }
    
    return $text;
  }
  

}
