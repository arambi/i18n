<?php
namespace I18n\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Utility\Inflector;

/**
 * Languages Model
 */
class LanguagesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize( array $config) 
	{
		$this->setTable( 'languages');
		$this->setDisplayField( 'name');
		$this->setPrimaryKey( 'id');

		// Behaviors
		$this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
		$this->addBehavior( 'Cofree.Sortable');

		$this->addBehavior( 'Cofree.BooleanUnique', [
      'fields' => [ 'by_default']
    ]);
    
		// CRUD
		$this->addBehavior( 'Manager.Crudable');

		$this->crud
			->addFields([
				'name' => 'Nombre',
				'iso2' => 'ISO2',
				'iso3' => 'ISO3',
				'locale' => 'Locale',
        'by_default' => 'Idioma por defecto',
				'published' => 'Activo',
			])

			->addIndex( 'index', [
				'fields' => [
					'name',
					'iso2',
					'iso3',
          'by_default',
					'published'
				],
				'actionButtons' => ['create'],
			])

			->setName( [
				'singular' => 'Idioma',
				'plural' => 'Idiomas',
			])

			->addView( 'update', [
				'columns' => [
					[
						'cols' => 8,
						'box' => [
							[
								'title' => 'Edición',
								'elements' => [
									'name',
									'iso2',
									'iso3',
									'locale',
                  'by_default',
									'published',
								]
							]
						]
					]
				],
				'actionButtons' => ['index', 'create'],
			], 'create');
	}

  public function beforeSave( Event $event, EntityInterface $entity, ArrayObject $options)
  {

  }

  public function afterSave( Event $event, EntityInterface $entity)
  {
  	if( $entity->isNew())
  	{
  		$lang = TableRegistry::get( 'I18n.Languages')->find()
  			->where(['by_default' => true])
  			->first();

  		if( !$lang)
  		{
  			return;
  		}

			if( Configure::read( 'I18n.behavior') != 'I18n.I18nTranslate')
			{
				return;
			}

			$models = [
				'Section.Sections'
			];

			foreach( $models as $model)
			{
				$table = TableRegistry::get( $model);
				$i18Table = TableRegistry::get( Inflector::camelize( $table->table() . '_translations'));
				
				$limit = 50;
				$offset = 0;
		
				while( true)
				{
					$contents = $i18Table->find()
					->where([
						'locale' => $lang->iso3
					])
					->limit( $limit)
					->offset( $offset)
					->order( 'id')
					->all();

					if( $contents->count() == 0)
					{
						break;
					}

					foreach( $contents as $content)
					{
						$_entity = $i18Table->find()
						->where([
							'id' => $content->id,
							'locale' => $entity->iso3
						])
						->first();
					
						if( $_entity)
						{
							continue;
						}

						$_entity = $i18Table->save( $i18Table->newEntity( $content->toArray()));
						$_entity->set( 'locale', $entity->iso3);
						$i18Table->save( $_entity);
					}

					$offset = $offset + $limit;
				}

				
			}

			
  		// $I18n = TableRegistry::get( 'I18n');

  		// $nodes = $I18n->find()
  		// 	->where([
  		// 		'locale' => $lang->iso3
  		// 	]);

  		// foreach( $nodes as $node)
  		// {
  		// 	$data = $node->toArray();

  		// 	unset( $data ['id']);
  		// 	unset( $data ['created']);
  		// 	unset( $data ['modified']);
  		// 	$data ['locale'] = $entity->iso3;

  		// 	if( $I18n->find()->where( $data)->count() == 0)
  		// 	{
  		// 		$_entity = $I18n->newEntity( $data);
  		// 		$I18n->save( $_entity);
  		// 	}
  		// }

  		$Slugs = TableRegistry::get( 'Slugs');

  		$nodes = $Slugs->find()
  			->where([
  				'locale' => $lang->iso3
  			]);

  		foreach( $nodes as $node)
  		{
  			$data = $node->toArray();

  			unset( $data ['id']);
  			unset( $data ['created']);
  			unset( $data ['modified']);
  			$data ['locale'] = $entity->iso3;

  			if( $Slugs->find()->where( $data)->count() == 0)
  			{
  				$_entity = $Slugs->newEntity( $data);
  				$Slugs->save( $_entity);
  			}
  		}
  	}
  }

}
