<?php
namespace I18n\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use I18n\Model\Entity\Dictionary;
use I18n\Lib\Lang;
use Cake\Cache\Cache;

/**
 * Dictionaries Model
 */
class TranslationsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table(null);
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        
      ])
      ->addIndex( 'index', [
        'template' => 'I18n/translations_index'
      ])
      ->setName( [
        'singular' => 'Traducción',
        'plural' => 'Traducción',
      ])
      
      ;
      
  }

  

}
