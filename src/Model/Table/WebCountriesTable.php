<?php
namespace I18n\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WebCountries Model
 *
 * @method \I18n\Model\Entity\WebCountry get($primaryKey, $options = [])
 * @method \I18n\Model\Entity\WebCountry newEntity($data = null, array $options = [])
 * @method \I18n\Model\Entity\WebCountry[] newEntities(array $data, array $options = [])
 * @method \I18n\Model\Entity\WebCountry|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \I18n\Model\Entity\WebCountry saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \I18n\Model\Entity\WebCountry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \I18n\Model\Entity\WebCountry[] patchEntities($entities, array $data, array $options = [])
 * @method \I18n\Model\Entity\WebCountry findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WebCountriesTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('i18n_web_countries');
    $this->setDisplayField('title');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Sortable');
    $this->addBehavior( 'Cofree.Publisher');
    $this->addBehavior( 'I18n.I18nTranslate', [
      'fields' => ['title']
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'published' => __d( 'admin', 'Activo'),
        'title' => __d( 'admin', 'Título'),
        'code' => __d( 'admin', 'Codigo'),
      ])
      ->addIndex( 'index', [
        'fields' => [
          'published',
          'title',
          'code',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Países del web'),
        'plural' => __d( 'admin', 'Países del web'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'key' => 'general',
            'cols' => 8,
            'box' => [
              [
                'key' => 'general',
                'elements' => [
                  'published',
                  'title',
                  'code',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ->defaults([
        'published' => true
      ])
      ;
  
  }
  
  public function findFront( Query $query)
  {
    return $query
      ->find( 'published')
    ;
  }

}
