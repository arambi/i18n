<?php
/**
 * Copyright 2009-2010, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2009-2010, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
namespace I18n\Routing\Route;

use Cake\Routing\Route\Route;
use Cake\I18n\I18n;
use Locale;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Log\Log;
use Cake\Utility\Inflector;
use I18n\Lib\Lang;
use Section\Routing\RouteData;

/**
 *
 *
 */
class I18nRoute extends Route {

/**
 * Internal flag to know whether default routes were mapped or not
 * 
 * @var boolean
 */ 
  protected $_mappedDefaults = false;

/**
 * Class name - Workaround to be able to extend this class without breaking existing features
 *
 * @var string
 */
  public $name = __CLASS__;


  public function __construct( $template, $defaults = array(), $options = array()) 
  {
    parent::__construct( $template, $defaults, $options);

    if( Configure::read( 'I18n.disable'))
    {
      return;
    }

    if( strpos( $this->template, ':lang') === false) 
    {
      $this->template = '/:lang' . $this->template;
    }

    $this->options = array_merge((array)$this->options, array(
      'lang' => join( '|', array_keys( Lang::keys( true))),
    ));

    if ($this->template == '/:lang/') 
    {
      $this->template = '/:lang';
    }
  }

  public function compile() 
  {
    $lang = substr( $this->template, 1, 2);

    if( !Configure::read( 'I18n.disable'))
    {
      $lang = substr( $this->template, 1, 2);
      $this->defaults ['lang'] = $lang;
    }

    parent::compile();
  }

/**
 * Attempt to match a url array.  If the url matches the route parameters + settings, then
 * return a generated string url.  If the url doesn't match the route parameters false will be returned.
 * This method handles the reverse routing or conversion of url arrays into string urls.
 *
 * @param array $url An array of parameters to check matching with.
 * @return mixed Either a string url for the parameters if they match or false.
 */
  public function match( array $url, array $context = []) 
  {
    $url = $this->_underscore( $url);

    if( empty( $url['lang'])) 
    {
      // $url ['lang'] = Locale::getPrimaryLanguage( I18n::locale());
      $url ['lang'] = Lang::current();
    }

    if( !Configure::read( 'I18n.disable') && empty( $url ['locale']))
    {
      $url ['locale'] = I18n::getLocale();
    }

    if (!$this->_mappedDefaults) 
    {
      $this->_mappedDefaults = true;
      $this->defaults = $this->_underscore($this->defaults);
      $this->defaults ['lang'] = $url ['lang'];
    }
    
    $result = parent::match( $url, $context);

    if( Configure::read( 'Country') && !empty( $result))
    {
      $result = '/'. Configure::read( 'Country') .'-' . substr( $result, 1);
    }

    return $result;
  }

/**
 * Checks to see if the given URL can be parsed by this route.
 * If the route can be parsed an array of parameters will be returned if not
 * false will be returned. String urls are parsed if they match a routes regular expression.
 *
 * @param string $url The url to attempt to parse.
 * @return mixed Boolean false on failure, otherwise an array or parameters
 * @access public
 */
  public function parse( $url, $method = '') 
  {
    $params = parent::parse( $url, $method);

    if( Configure::read( 'I18n.disable'))
    {
      I18n::setLocale( Lang::getIso3( Configure::read( 'I18n.default')));
    }
    elseif ($params !== false && array_key_exists('lang', $params)) 
    {
      $params ['lang'] = empty( $params['lang']) ? Lang::getIso2(I18n::getLocale()) : $params['lang'];
      // I18n::setLocale( Lang::getIso3( $params['lang']));
      // setlocale( LC_ALL, Lang::current( 'locale') .'.UTF-8');
    }
    else
    {
      Lang::getCurrent( true);
    }


    RouteData::current( $this);
    return $params;
  }

  /**
 * Helper method for underscoring keys in a URL array.
 *
 * @param array $url An array of URL keys.
 * @return array
 */
  protected function _underscore( $url) 
  {
    if( !empty( $url ['controller'])) 
    {
      $url ['controller'] = Inflector::underscore( $url ['controller']);
    }

    if( !empty( $url ['plugin'])) 
    {
      $url ['plugin'] = Inflector::underscore( $url ['plugin']);
    }

    return $url;
  }

}