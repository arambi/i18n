<?php
namespace I18n\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use I18n\I18n\LangCollection;
use I18n\Lib\Lang;

/**
 * Dumper shell command.
 */
class DumperShell extends Shell
{

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $this->createLanguages();
  }

  public function createLanguages()
  {
    $locales = $this->__getLangs();

    $Languages = TableRegistry::get( 'I18n.Languages');

    foreach( $locales as $key => $record)
    {
      if( $key == 0)
      {
        $record ['by_default'] = 1;
      }

      $entity = $Languages->getNewEntity( $record);

      if( $Languages->save( $entity))
      {
        $this->out( 'Creado el idioma '. $entity->name);
      }
    }

    Lang::reload();
  }

  private function __getLangs()
  {
    $langs = $this->in( 'Indica, separado por comas, los idiomas en formato de dos letras', false, 'es');
    $locales = explode( ',', $langs);

    $return = [];

    foreach( $locales as $locale)
    {
      if( $info = LangCollection::get( $locale))
      {
        $return [] = $info;
      }
    }

    if( empty( $return))
    {
      $this->__getLangs();
    }

    return $return;
  }

}