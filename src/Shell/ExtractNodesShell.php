<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         1.2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace I18n\Shell;

use Cake\Console\Shell;
use Cake\Core\App;
use Cake\Core\Plugin;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

/**
 * Language string extractor
 *
 */
class ExtractNodesShell extends Shell
{

    /**
     * Paths to use when looking for strings
     *
     * @var array
     */
    protected $_paths = [];

    /**
     * Files from where to extract
     *
     * @var array
     */
    protected $_files = [];

    /**
     * Merge all domain strings into the default.pot file
     *
     * @var bool
     */
    protected $_merge = false;

    /**
     * Current file being processed
     *
     * @var string
     */
    protected $_file = null;

    /**
     * Contains all content waiting to be write
     *
     * @var array
     */
    protected $_storage = [];

    /**
     * Extracted tokens
     *
     * @var array
     */
    protected $_tokens = [];

    /**
     * Extracted strings indexed by domain.
     *
     * @var array
     */
    protected $_translations = [];

    /**
     * Destination path
     *
     * @var string
     */
    protected $_output = null;

    /**
     * An array of directories to exclude.
     *
     * @var array
     */
    protected $_exclude = [];

    /**
     * Holds the validation string domain to use for validation messages when extracting
     *
     * @var bool
     */
    protected $_validationDomain = 'default';

    /**
     * Holds whether this call should extract the CakePHP Lib messages
     *
     * @var bool
     */
    protected $_extractCore = false;



    public function initialize()
    {
      parent::initialize();
      $this->Dictionaries = TableRegistry::get( 'I18n.Dictionaries');
    }

    /**
     * No welcome message.
     *
     * @return void
     */
    protected function _welcome()
    {
    }

    /**
     * Method to interact with the User and get path selections.
     *
     * @return void
     */
    protected function _getPaths()
    {
        $this->_paths[] = ROOT .DS. 'vendor' .DS. 'cofreeweb';
        $this->_paths[] = ROOT .DS. 'plugins';
        $this->_paths[] = ROOT .DS. 'src';
    }

    /**
     * Execution method always used for tasks
     *
     * @return void
     */
    public function main()
    {
        if (!empty($this->params['exclude'])) {
            $this->_exclude = explode(',', $this->params['exclude']);
        }
        if (isset($this->params['files']) && !is_array($this->params['files'])) {
            $this->_files = explode(',', $this->params['files']);
        }
        if (isset($this->params['paths'])) {
            $this->_paths = explode(',', $this->params['paths']);
        } elseif (isset($this->params['plugin'])) {
            $plugin = Inflector::camelize($this->params['plugin']);
            if (!Plugin::loaded($plugin)) {
                Plugin::load($plugin);
            }
            $this->_paths = [Plugin::classPath($plugin)];
            $this->params['plugin'] = $plugin;
        } else {
            $this->_getPaths();
        }

        $this->_extractCore = false;


        if ($this->_extractCore) {
            $this->_paths[] = CAKE;
        }

       

        if (empty($this->_files)) {
            $this->_searchFiles();
        }

      
        $this->_extract();
    }

    /**
     * Add a translation to the internal translations property
     *
     * Takes care of duplicate translations
     *
     * @param string $domain The domain
     * @param string $msgid The message string
     * @param array $details Context and plural form if any, file and line references
     * @return void
     */
    protected function _addTranslation($domain, $msgid, $details = [])
    {
        $context = isset($details['msgctxt']) ? $details['msgctxt'] : "";

        if (empty($this->_translations[$domain][$msgid][$context])) {
            $this->_translations[$domain][$msgid][$context] = [
                'msgid_plural' => false
            ];
        }

        if (isset($details['msgid_plural'])) {
            $this->_translations[$domain][$msgid][$context]['msgid_plural'] = $details['msgid_plural'];
        }

        if (isset($details['file'])) {
            $line = isset($details['line']) ? $details['line'] : 0;
            $this->_translations[$domain][$msgid][$context]['references'][$details['file']][] = $line;
        }
    }

    /**
     * Extract text
     *
     * @return void
     */
    protected function _extract()
    {
        $this->out();
        $this->out();
        $this->out('Extracting...');
        $this->hr();
        $this->out('Paths:');
        foreach ($this->_paths as $path) {
            $this->out('   ' . $path);
        }
        $this->out('Output Directory: ' . $this->_output);
        $this->hr();
        $this->_extractTokens();
        $this->_saveNodes();
        $this->out();
        $this->out('Done.');
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->description(
            'CakePHP Language String Extraction:'
        )->addOption('app', [
            'help' => 'Directory where your application is located.'
        ])->addOption('paths', [
            'help' => 'Comma separated list of paths.'
        ])->addOption('merge', [
            'help' => 'Merge all domain strings into the default.po file.',
            'choices' => ['yes', 'no']
        ])->addOption('output', [
            'help' => 'Full path to output directory.'
        ])->addOption('files', [
            'help' => 'Comma separated list of files.'
        ])->addOption('exclude-plugins', [
            'boolean' => true,
            'default' => true,
            'help' => 'Ignores all files in plugins if this command is run inside from the same app directory.'
        ])->addOption('plugin', [
            'help' => 'Extracts tokens only from the plugin specified and puts the result in the plugin\'s Locale directory.'
        ])->addOption('ignore-model-validation', [
            'boolean' => true,
            'default' => false,
            'help' => 'Ignores validation messages in the $validate property.' .
                ' If this flag is not set and the command is run from the same app directory,' .
                ' all messages in model validation rules will be extracted as tokens.'
        ])->addOption('validation-domain', [
            'help' => 'If set to a value, the localization domain to be used for model validation messages.'
        ])->addOption('exclude', [
            'help' => 'Comma separated list of directories to exclude.' .
                ' Any path containing a path segment with the provided values will be skipped. E.g. test,vendors'
        ])->addOption('overwrite', [
            'boolean' => true,
            'default' => false,
            'help' => 'Always overwrite existing .pot files.'
        ])->addOption('extract-core', [
            'help' => 'Extract messages from the CakePHP core libs.',
            'choices' => ['yes', 'no']
        ])->addOption('no-location', [
            'boolean' => true,
            'default' => false,
            'help' => 'Do not write file locations for each extracted message.',
        ]);

        return $parser;
    }

    /**
     * Extract tokens out of all files to be processed
     *
     * @return void
     */
    protected function _extractTokens()
    {
        $progress = $this->helper('progress');
        $progress->init(['total' => count($this->_files)]);
        $isVerbose = $this->param('verbose');

        foreach ($this->_files as $file) {
            $this->_file = $file;
            if ($isVerbose) {
                $this->out(sprintf('Processing %s...', $file), 1, Shell::VERBOSE);
            }

            $code = file_get_contents($file);
            $allTokens = token_get_all($code);

            $this->_tokens = [];
            foreach ($allTokens as $token) {
                if (!is_array($token) || ($token[0] !== T_WHITESPACE && $token[0] !== T_INLINE_HTML)) {
                    $this->_tokens[] = $token;
                }
            }
            unset($allTokens);
            $this->_parse('__', ['singular']);
            $this->_parse('__n', ['singular', 'plural']);
            $this->_parse('__d', ['domain', 'singular']);
            $this->_parse('__dn', ['domain', 'singular', 'plural']);
            $this->_parse('__x', ['context', 'singular']);
            $this->_parse('__xn', ['context', 'singular', 'plural']);
            $this->_parse('__dx', ['domain', 'context', 'singular']);
            $this->_parse('__dxn', ['domain', 'context', 'singular', 'plural']);

            if (!$isVerbose) {
                $progress->increment(1);
                $progress->draw();
            }
        }

    }

    /**
     * Parse tokens
     *
     * @param string $functionName Function name that indicates translatable string (e.g: '__')
     * @param array $map Array containing what variables it will find (e.g: domain, singular, plural)
     * @return void
     */
    protected function _parse($functionName, $map)
    {
        $count = 0;
        $tokenCount = count($this->_tokens);

        while (($tokenCount - $count) > 1) {
            $countToken = $this->_tokens[$count];
            $firstParenthesis = $this->_tokens[$count + 1];
            if (!is_array($countToken)) {
                $count++;
                continue;
            }

            list($type, $string, $line) = $countToken;
            if (($type == T_STRING) && ($string === $functionName) && ($firstParenthesis === '(')) {
                $position = $count;
                $depth = 0;

                while (!$depth) {
                    if ($this->_tokens[$position] === '(') {
                        $depth++;
                    } elseif ($this->_tokens[$position] === ')') {
                        $depth--;
                    }
                    $position++;
                }

                $mapCount = count($map);
                $strings = $this->_getStrings($position, $mapCount);

                if ($mapCount === count($strings)) {
                    extract(array_combine($map, $strings));
                    $domain = isset($domain) ? $domain : 'default';
                    $details = [
                        'file' => $this->_file,
                        'line' => $line,
                    ];
                    if (isset($plural)) {
                        $details['msgid_plural'] = $plural;
                    }
                    if (isset($context)) {
                        $details['msgctxt'] = $context;
                    }
                    $this->_addTranslation($domain, $singular, $details);
                } elseif (strpos($this->_file, CAKE_CORE_INCLUDE_PATH) === false) {
                    $this->_markerError($this->_file, $line, $functionName, $count);
                }
            }
            $count++;
        }
    }

    /**
     * Build the translate template file contents out of obtained strings
     *
     * @return void
     */
    protected function _saveNodes()
    {
      foreach( $this->_translations as $domain => $nodes)
      {
        foreach( $nodes as $node => $info)
        {
          $plural = isset( $info ['msgid_plural']) ? $info ['msgid_plural'] : false;
          $this->Dictionaries->add( $node, $domain, $plural);
        }
      }
    }

    /**
     * Prepare a file to be stored
     *
     * @param string $domain The domain
     * @param string $header The header content.
     * @param string $sentence The sentence to store.
     * @return void
     */
    protected function _store($domain, $header, $sentence)
    {
        if (!isset($this->_storage[$domain])) {
            $this->_storage[$domain] = [];
        }
        if (!isset($this->_storage[$domain][$sentence])) {
            $this->_storage[$domain][$sentence] = $header;
        } else {
            $this->_storage[$domain][$sentence] .= $header;
        }
    }

  
    /**
     * Get the strings from the position forward
     *
     * @param int $position Actual position on tokens array
     * @param int $target Number of strings to extract
     * @return array Strings extracted
     */
    protected function _getStrings(&$position, $target)
    {
        $strings = [];
        $count = count($strings);
        while ($count < $target && ($this->_tokens[$position] === ',' || $this->_tokens[$position][0] == T_CONSTANT_ENCAPSED_STRING || $this->_tokens[$position][0] == T_LNUMBER)) {
            $count = count($strings);
            if ($this->_tokens[$position][0] == T_CONSTANT_ENCAPSED_STRING && $this->_tokens[$position + 1] === '.') {
                $string = '';
                while ($this->_tokens[$position][0] == T_CONSTANT_ENCAPSED_STRING || $this->_tokens[$position] === '.') {
                    if ($this->_tokens[$position][0] == T_CONSTANT_ENCAPSED_STRING) {
                        $string .= $this->_formatString($this->_tokens[$position][1]);
                    }
                    $position++;
                }
                $strings[] = $string;
            } elseif ($this->_tokens[$position][0] == T_CONSTANT_ENCAPSED_STRING) {
                $strings[] = $this->_formatString($this->_tokens[$position][1]);
            } elseif ($this->_tokens[$position][0] == T_LNUMBER) {
                $strings[] = $this->_tokens[$position][1];
            }
            $position++;
        }
        return $strings;
    }

    /**
     * Format a string to be added as a translatable string
     *
     * @param string $string String to format
     * @return string Formatted string
     */
    protected function _formatString($string)
    {
        $quote = substr($string, 0, 1);
        $string = substr($string, 1, -1);
        if ($quote === '"') {
            $string = stripcslashes($string);
        } else {
            $string = strtr($string, ["\\'" => "'", "\\\\" => "\\"]);
        }
        $string = str_replace("\r\n", "\n", $string);
        return addcslashes($string, "\0..\37\\\"");
    }

    /**
     * Indicate an invalid marker on a processed file
     *
     * @param string $file File where invalid marker resides
     * @param int $line Line number
     * @param string $marker Marker found
     * @param int $count Count
     * @return void
     */
    protected function _markerError($file, $line, $marker, $count)
    {
        $this->err(sprintf("Invalid marker content in %s:%s\n* %s(", $file, $line, $marker));
        $count += 2;
        $tokenCount = count($this->_tokens);
        $parenthesis = 1;

        while ((($tokenCount - $count) > 0) && $parenthesis) {
            if (is_array($this->_tokens[$count])) {
                $this->err($this->_tokens[$count][1], false);
            } else {
                $this->err($this->_tokens[$count], false);
                if ($this->_tokens[$count] === '(') {
                    $parenthesis++;
                }

                if ($this->_tokens[$count] === ')') {
                    $parenthesis--;
                }
            }
            $count++;
        }
        $this->err("\n", true);
    }

    /**
     * Search files that may contain translatable strings
     *
     * @return void
     */
    protected function _searchFiles()
    {
        $pattern = false;
        if (!empty($this->_exclude)) {
            $exclude = [];
            foreach ($this->_exclude as $e) {
                if (DS !== '\\' && $e[0] !== DS) {
                    $e = DS . $e;
                }
                $exclude[] = preg_quote($e, '/');
            }
            $pattern = '/' . implode('|', $exclude) . '/';
        }
        foreach ($this->_paths as $path) {
            $Folder = new Folder($path);
            $files = $Folder->findRecursive('.*\.(php|ctp|thtml|inc|tpl)', true);
            if (!empty($pattern)) {
                foreach ($files as $i => $file) {
                    if (preg_match($pattern, $file)) {
                        unset($files[$i]);
                    }
                }
                $files = array_values($files);
            }
            $this->_files = array_merge($this->_files, $files);
        }
    }

    /**
     * Returns whether this execution is meant to extract string only from directories in folder represented by the
     * APP constant, i.e. this task is extracting strings from same application.
     *
     * @return bool
     */
    protected function _isExtractingApp()
    {
        return $this->_paths === [APP];
    }

    /**
     * Checks whether or not a given path is usable for writing.
     *
     * @param string $path Path to folder
     * @return bool true if it exists and is writable, false otherwise
     */
    protected function _isPathUsable($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0770, true);
        }
        return is_dir($path) && is_writable($path);
    }
}
