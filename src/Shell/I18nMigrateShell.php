<?php
namespace I18n\Shell;

use Cake\Console\Shell;
use Cake\Utility\Inflector;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * I18nMigrate shell command.
 */
class I18nMigrateShell extends Shell
{

  public function initialize()
  {
    parent::initialize();
    $this->parser = parent::getOptionParser();
  }

  public function migrate()
  { 
    Configure::write( 'I18n.behavior', 'I18n.I18nable');
    
    if( !empty( $this->params ['model']))
    {
      $model = $this->params ['model'];
    }
    else
    {
      $model = $this->in( 'Indica un model (plugin.model)');
    }

    $this->Table = $this->loadModel( $model);
    $fields = $this->Table->translateFields();

    $i18n_table = $this->Table->translateTable();
    $this->I18n = $this->loadModel( $i18n_table);
    $this->Shadow = $this->loadModel( Inflector::camelize( $this->Table->table() . '_translations'));

    $limit = 50;
    $offset = 0;

    while( true)
    {
      $contents = $this->I18n->find()
        ->where([
          'model' => $this->Table->alias()
        ])
        ->limit( $limit)
        ->offset( $offset)
        ->order( 'id')
        ->all();
      
      if( $contents->count() == 0)
      {
        break;
      }
      
      foreach( $contents as $content)
      {
        $entity = $this->Shadow->find()
          ->where([
            'id' => $content->foreign_key,
            'locale' => $content->locale
          ])
          ->first();

        if( !$entity)
        {
          $entity = $this->Shadow->save( $this->Shadow->newEntity([
            'id' => $content->foreign_key,
            'locale' => $content->locale
          ]));
        }

        $entity->set( $content->field, $content->content);
        $entity->dirty( $content->field, true);

        if( !$this->Shadow->save( $entity))
        {
          _d( 'Error al guardar');
        }


      }

      $offset = $offset + $limit;
    }     
  }

  public function core()
  {
    $models = [
      'Block.Blocks',
      'Block.Tabs',
      'Blog.Posts',
      'Blog.Events',
      'Blog.Authors',
      'Form.Fields',
      'Form.Forms',
      'Form.Options',
      'Letter.Letters',
      'Section.Sections',
      'Section.Pieces',
      'Section.Links',
      'Seo.Seos',
      'Slideshow.Slides',
      'Taxonomy.Categories',
      'Website.Sites',
    ];
    
    if( Configure::read( 'I18n.tableMigrateExceptions'))
    {
      $exceptions = Configure::read( 'I18n.tableMigrateExceptions');
    }
    else
    {
      $exceptions = [];
    }
    
    foreach( $models as $model)
    {
      if( in_array( $model, $exceptions))
      {
        continue;
      }
      
      $this->out( 'Comenzando migración para '. $model);
      $this->dispatchShell( [
        'command' => 'i18n_migrate migrate',
        'extra' => ['model' => $model]
      ]);
    }
  }


  public function all()
  {
    $this->core();

    if( Configure::read( 'I18n.tableMigrate'))
    {
      foreach( Configure::read( 'I18n.tableMigrate') as $model)
      {
        $this->out( 'Comenzando migración para '. $model);
        $this->dispatchShell( [
          'command' => 'i18n_migrate migrate',
          'extra' => ['model' => $model]
        ]);
      }
    }
  }

  public function getOptionParser()
  {
    $parser = $this->parser;
    $parser->description(
        'Migrate I18n'
    ) 
      ->addSubcommand( 'migrate', [
          'help' => 'Realiza una migración de datos a ShadowTranslate',
      ])
      ->addSubcommand( 'core', [
          'help' => 'Realiza una migración de datos del CORE',
      ])
      ->addSubcommand( 'all', [
          'help' => 'Realiza una migración completa',
      ])
      ->addOption( 'model', [
        'short' => 'm',
        'help' => 'Model en formato Plugin.Model',
        'default' => false,
      ])
     ;
    return $parser;
  }
}
