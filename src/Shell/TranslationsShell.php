<?php

namespace I18n\Shell;

use I18n\Lib\Lang;
use Cake\Core\Plugin;
use Cake\Console\Shell;
use Cake\Core\Configure;
use I18n\Lib\Translator;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cofree\Lib\PluginUtils;
use Cake\Datasource\ConnectionManager;
use I18n\Lib\GoogleTranslator;
use I18n\Lib\TranslatorCsv;

/**
 * Translations shell command.
 */
class TranslationsShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function extract()
    {
        $paths = [
            ROOT . '/plugins',
            ROOT . '/src',
            ROOT . '/vendor/cofreeweb/form/src',
            ROOT . '/vendor/cofreeweb/user/src',
            ROOT . '/config',
        ];

        if (Plugin::loaded('Store')) {
            $paths[] = ROOT . '/vendor/arambi/store/src';
        }

        if (Plugin::loaded('Retail')) {
            $paths[] = ROOT . '/vendor/funtsak/retail/src';
        }

        $this->dispatchShell([
            'command' => 'i18n extract --paths ' . implode(',', $paths) . ' --output ' . ROOT . '/src/Locale --overwrite yes --extract-core no --merge no'
        ]);
    }

    public function addField()
    {
        $current_lang = Lang::current('iso3');
        $model = $this->in('Indica un model (plugin.model)');
        $fields = $this->in('Indica el campo');
        $table = TableRegistry::get($model);
        $translationTableName = Inflector::camelize($table->table() . '_translations');
        $translationTable = TableRegistry::get($translationTableName);

        $limit = 50;
        $offset = 0;

        while (true) {
            $contents = $table->find()
                ->limit($limit)
                ->offset($offset)
                ->order($table->alias() . '.id')
                ->all();

            if ($contents->count() == 0) {
                break;
            }

            foreach ($contents as $content) {
                $shadows = $translationTable->find()
                    ->where([
                        'id' => $content->id
                    ]);

                if ($shadows->count() == 0) {
                    foreach (Lang::iso3() as $locale => $name) {
                        $translationTable->save($translationTable->newEntity([
                            'id' => $content->id,
                            'locale' => $locale
                        ]));
                    }
                }

                $shadows = $translationTable->find()
                    ->where([
                        'id' => $content->id
                    ]);


                foreach ($shadows as $shadow) {
                    if ($shadow->locale == $current_lang) {
                        foreach (explode(',', $fields) as $field) {
                            $value = $content->$field;
                            $shadow->set($field, $value);
                        }

                        $translationTable->save($shadow);
                    }
                }
            }

            $offset = $offset + $limit;
        }
    }

    public function inverse()
    {
        $model = $this->in('Indica un model (plugin.model)');
        $field = $this->in('Indica el campo');
        $locale = $this->in('Indica el locale (3 letras)');

        $table = TableRegistry::get($model);
        $translationTableName = Inflector::camelize($table->table() . '_translations');
        $translationTable = TableRegistry::get($translationTableName);

        $contents = $table->find()
            ->where([]);

        foreach ($contents as $content) {
            $translation = $translationTable->find()
                ->where([
                    'locale' => $locale,
                    'id' => $content->id
                ])
                ->first();

            $table->query()->update()
                ->where([
                    'id' => $content->id,
                ])
                ->set([
                    $field => $translation->get($field)
                ])
                ->execute();
        }
    }

    public function generate()
    {
        $models = Configure::read('I18n.models');
        $translator = new Translator($models);
        $translator->write();
    }

    public function read()
    {
        $doc = new \PhpOffice\PhpWord\PhpWord();
        $doc->loadTemplate(TMP . 'translates_eus.docx');
    }

    public function changeLocale()
    {
        $connection = ConnectionManager::get('default');

        $from = $this->in('Idioma antiguo');
        $to = $this->in('Idioma nuevo');

        $tables = $this->getTables();

        foreach ($tables as $tableName => $fields) {
            $table = TableRegistry::getTableLocator()->get($tableName);
            $transTable = $table->translationTable();
            $transTableName = $transTable->getTable();
            $results = $connection->execute("SHOW TABLES LIKE '$transTableName'")->fetchAll('assoc');

            if (empty($results)) {
                continue;
            }

            $transTable->updateAll([
                'locale' => $to
            ], [
                'locale' => $from
            ]);
        }

        $this->loadModel('Slug.Slugs')->updateAll([
            'locale' => $to
        ], [
            'locale' => $from
        ]);
    }

    public function removeLocale()
    {
        $connection = ConnectionManager::get('default');
        $locale = $this->in('Idioma a eliminar');

        $tables = $this->getTables();

        foreach ($tables as $tableName => $fields) {
            $table = TableRegistry::getTableLocator()->get($tableName);
            $transTable = $table->translationTable();
            $transTableName = $transTable->getTable();
            $results = $connection->execute("SHOW TABLES LIKE '$transTableName'")->fetchAll('assoc');

            if (empty($results)) {
                continue;
            }

            $transTable->deleteAll([
                'locale' => $locale
            ]);
        }

        $this->loadModel('Slug.Slugs')->deleteAll([
            'locale' => $locale
        ]);
    }

    private function getTables()
    {
        $tables = [];
        $plugins = Plugin::loaded();

        foreach ($plugins as $plugin) {
            $models = PluginUtils::getModels($plugin);

            foreach ($models as $model) {
                $table = TableRegistry::getTableLocator()->get($plugin . '.' . $model);
                $pluginModel = $plugin . '.' . $model;

                if ($table->hasBehavior('I18nTranslate')) {
                    $fields = $table->translateFields();

                    if (isset($tables[$pluginModel])) {
                        $tables[$pluginModel] = $tables[$pluginModel] + $fields;
                        $tables[$pluginModel] = array_unique($tables[$pluginModel]);
                    } else {
                        $tables[$pluginModel] = $fields;
                    }
                }
            }
        }

        return $tables;
    }

    public function word()
    {
        $translator = new TranslatorCsv();
        $translator->run();
    }

    public function google()
    {
        $translator = new GoogleTranslator();
        $translator->setMainLang('spa')->run();
        $this->out($translator->getCharsTotal());
    }
}
