<div class="row wrapper wrapper-index border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div class="wrapper animated fadeIn">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">

          <div ng-if="data.contents.length == 0">
            <?= __d( 'admin', 'Todavía no hay ningún contenido creado para {0}.', ['{{ data.crudConfig.name.plural }}']) ?>
            <div class="m-t">
              <a class="btn btn-default" href="#{{ data.crudConfig.adminUrl }}create"><?= __d( 'admin', 'Crear {0}', ['{{ data.crudConfig.name.singular }}']) ?></a>
            </div>
          </div>
          <div class="table-responsive">

            <table class="table table-striped table-bordered table-hover dataTables-example" sortable-options="data.contents" config="data.crudConfig">
              <thead>
                <!-- Encabezados -->
                <tr>
                  <th></th>
                  <th ng-repeat="language in data.languages">
                    {{ language.name }}
                  </th>
                  <th></th>
                </tr>
                <!-- /Encabezados -->

              </thead>
            </table>
            <div ng-include="'content_index.html'"></div>
            
          </div>
          <cf-paginator></cf-paginator>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Nested node template -->
<script type="text/ng-template" id="content_index.html">
  <div ng-repeat="content in data.contents">
    <table class="table table-striped table-bordered table-hover dataTables-example" sortable-options="data.contents" config="data.crudConfig">
      <tr>
        <th></th>
        <th ng-repeat="language in data.languages">
          {{ language.name }}
        </th>
        <th></th>
      </tr>
      <tr ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">
        <td>{{ field.label }}</td>
        <td ng-repeat="language in data.languages">
          <div ng-model="content" cf-modal-update="'/admin/i18n/translations/translate.json?model=' + data.crudConfig.query.model + '&field=' + field.key + '&lang=' + language.iso3">{{ content._translations[language.iso3][field.key] }}</div>
        </td>
        <td>
          <span cf-action-link="'/admin/i18n/translations/update/' + data.crudConfig.table + '/' + data.crudConfig.locale" params="{ content: content, table: data.crudConfig.table   }" class="btn btn-primary btn-sm "><?= __d( 'admin', 'Guardar') ?></span>
        </td>
      </tr>
    </table>
  </div> 
</script>
