<?php
namespace I18n\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * WebCountries helper
 */
class WebCountriesHelper extends Helper
{
 
  protected $_defaultConfig = [];

  public function nav()
  {
    $countries = TableRegistry::get( 'I18n.WebCountries')->find()
      ->where([
        'WebCountries.code !=' => Configure::read( 'Country')
      ])
      ->order([
        'WebCountries.title'
      ]);

    $out = [];

    foreach( $countries as $country)
    {
      $url = '/'. strtolower( $country->code) . substr( env('REQUEST_URI'), 3);
      $out [] = '<li><a href="'. $url .'">'. $country->title .'</a></li>';
    }

    return '<ul>'. implode( "\n", $out) .'</ul>';
  }

  public function current()
  {
    if( Configure::read( 'Country'))
    {
      $country = TableRegistry::get( 'I18n.WebCountries')->find()
        ->where([
          'code' => Configure::read( 'Country')
        ])
        ->first();

      return $country;
    }
  }

}
