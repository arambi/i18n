<?php
namespace I18n\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LanguagesFixture
 *
 */
class LanguagesFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'name' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'iso2' => ['type' => 'string', 'length' => 6, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'iso3' => ['type' => 'string', 'length' => 3, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'locale' => ['type' => 'string', 'length' => 6, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'by_default' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'position' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			'iso2' => ['type' => 'index', 'columns' => ['iso2'], 'length' => []],
			'iso3' => ['type' => 'index', 'columns' => ['iso3'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'name' => 'Español',
			'iso2' => 'es',
			'iso3' => 'spa',
			'locale' => 'es_ES',
			'by_default' => 1,
			'position' => 1,
			'created' => '2014-11-13 19:12:07',
			'modified' => '2014-11-13 19:12:07'
		],
		[
			'id' => 2,
			'name' => 'English',
			'iso2' => 'en',
			'iso3' => 'eng',
			'locale' => 'en_UK',
			'by_default' => 0,
			'position' =>5,
			'created' => '2014-11-13 19:12:07',
			'modified' => '2014-11-13 19:12:07'
		],
	];

}
