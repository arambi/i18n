<?php
namespace I18n\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use I18n\Controller\Admin\DictionariesController;

/**
 * I18n\Controller\Admin\DictionariesController Test Case
 */
class DictionariesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.i18n.dictionaries'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
