<?php
namespace I18n\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use I18n\Controller\Admin\WebCountriesController;

/**
 * I18n\Controller\Admin\WebCountriesController Test Case
 *
 * @uses \I18n\Controller\Admin\WebCountriesController
 */
class WebCountriesControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.I18n.WebCountries',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
