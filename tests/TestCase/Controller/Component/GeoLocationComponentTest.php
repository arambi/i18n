<?php
namespace I18n\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use I18n\Controller\Component\GeoLocationComponent;

/**
 * I18n\Controller\Component\GeoLocationComponent Test Case
 */
class GeoLocationComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \I18n\Controller\Component\GeoLocationComponent
     */
    public $GeoLocation;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->GeoLocation = new GeoLocationComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GeoLocation);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
