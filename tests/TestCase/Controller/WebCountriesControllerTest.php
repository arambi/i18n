<?php
namespace I18n\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use I18n\Controller\WebCountriesController;

/**
 * I18n\Controller\WebCountriesController Test Case
 *
 * @uses \I18n\Controller\WebCountriesController
 */
class WebCountriesControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.I18n.WebCountries',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
