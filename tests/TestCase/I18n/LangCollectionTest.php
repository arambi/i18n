<?php
namespace I18n\Test\TestCase\I18n\I18n\LangCollection;

use Cake\TestSuite\TestCase;
use I18n\I18n\LangCollection;

class LanguagesTableTest extends TestCase 
{

  public function testGet()
  {
    $locale = LangCollection::get( 'spa');
    $this->assertEquals( 'Español', $locale ['name']);
    $this->assertEquals( 'es', $locale ['iso2']);
    $this->assertEquals( 'spa', $locale ['iso3']);
    $this->assertEquals( 'es_es', $locale ['locale']);
  }

}
