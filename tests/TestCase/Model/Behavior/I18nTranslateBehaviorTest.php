<?php
namespace I18n\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use I18n\Model\Behavior\I18nTranslateBehavior;

/**
 * I18n\Model\Behavior\I18nTranslateBehavior Test Case
 */
class I18nTranslateBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \I18n\Model\Behavior\I18nTranslateBehavior
     */
    public $I18nTranslate;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->I18nTranslate = new I18nTranslateBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->I18nTranslate);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
