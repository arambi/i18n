<?php
namespace I18n\Test\TestCase\Model\Behavior;

use I18n\Model\Behavior\I18nableBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\TestSuite\TestCase;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\I18n\I18n;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;

class ContentsTable extends Table 
{

	public function initialize( array $options) 
  {
  	$this->entityClass( 'I18n\Test\TestCase\Model\Behavior\Content');
		$this->addBehavior( 'I18n.I18nable', ['fields' => ['title']]);
		$this->addBehavior( 'Timestamp');
		$this->validator()
			->add( 'id', 'valid', ['rule' => 'numeric'])
			->allowEmpty( 'id', 'create')
			->requirePresence( 'title', 'create')
			->notEmpty( 'title');
	}

}

class Content extends Entity 
{
  use TranslateTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'title' => true,
	];
}

/**
 * I18n\Model\Behavior\I18nableBehavior Test Case
 */
class I18nableBehaviorTest extends TestCase 
{

	public $fixtures = [
    'plugin.i18n.contents',
    'plugin.manager.translates'
  ];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() 
	{
		parent::setUp();
		$this->connection = ConnectionManager::get( 'test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() 
	{
		unset($this->Contents);

		parent::tearDown();
	}

/**
 * Verifica el correcto funcionamiento de I18nableBehavior::translateFields()
 */
	public function testTranslateFields()
	{
		$fields = $this->Contents->translateFields();
		$this->assertSame( $fields, ['title']);
	}

/**
 * Verifica el correcto funcionamiento de I18nableBehavior::addTranslateFields()
 */
  public function testAddTranslateFields()
  {
    $this->Contents->addTranslateFields(['body']);
    $fields = $this->Contents->translateFields();
    $this->assertSame( $fields, ['title', 'body']);
  }

/**
 * Verifica que se cambia el campo de la búsqueda en caso de que sea de traducción
 */
  public function testFind()
  {
    I18n::locale( 'eng');
    $content = $this->Contents->find()
      ->where(['Contents_title_translation.content LIKE' => '%Hello%'])
      ->first();
    
    $this->assertEquals( 1, $content->id);
  }

}
