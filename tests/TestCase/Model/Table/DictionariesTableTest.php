<?php
namespace I18n\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use I18n\Model\Table\DictionariesTable;
use Manager\TestSuite\CrudTestCase;
use Cake\I18n\I18n;
use I18n\Lib\Lang;

/**
 * I18n\Model\Table\DictionariesTable Test Case
 */
class DictionariesTableTest extends CrudTestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
      'plugin.i18n.dictionaries',
      'plugin.i18n.languages',
      'plugin.i18n.translates',
      'plugin.i18n.translates_i18n',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      I18n::locale( 'spa');
      $config = TableRegistry::exists('Dictionaries') ? [] : ['className' => 'I18n\Model\Table\DictionariesTable'];
      $this->Dictionaries = TableRegistry::get('Dictionaries', $config);
      $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }


/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown()
  {
      unset($this->Dictionaries);
      unset($this->Languages);

      parent::tearDown();
  }

/**
 * Test initialize method
 *
 * @return void
 */
      
  public function testConfig()
  {
    $this->assertCrudDataEdit( 'update', $this->Dictionaries);
    $this->assertCrudDataIndex( 'index', $this->Dictionaries);
  }

  public function testAdd()
  {
    $this->setLanguages();
    $entity = $this->Dictionaries->add( 'Hola amigos del mundo', 'default');
    
    $new = $this->Dictionaries->find()
      ->where(['id' => $entity->id])
      ->first();
    
    $this->assertEquals( 'Hola amigos del mundo', $new->text);

    $entity = $this->Dictionaries->add( 'Hola amigos del mundo', 'default');
    $this->assertEquals( $entity->id, $new->id);
  }

  public function testGetAll()
  {
    $nodes = $this->Dictionaries->getAll();
  }

}
