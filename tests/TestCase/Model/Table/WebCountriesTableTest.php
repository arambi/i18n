<?php
namespace I18n\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use I18n\Model\Table\WebCountriesTable;
use Manager\TestSuite\CrudTestCase;


/**
 * I18n\Model\Table\WebCountriesTable Test Case
 */
class WebCountriesTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \I18n\Model\Table\WebCountriesTable
     */
    public $WebCountries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.I18n.WebCountries',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WebCountries') ? [] : ['className' => WebCountriesTable::class];
        $this->WebCountries = TableRegistry::getTableLocator()->get('WebCountries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WebCountries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->WebCountries);
        $this->assertCrudDataIndex( 'index', $this->WebCountries);
      }
}
