<?php
namespace I18n\Test\TestCase\Shell;

use Cake\TestSuite\TestCase;
use I18n\Shell\DictionariesShell;

/**
 * I18n\Shell\DictionariesShell Test Case
 */
class DictionariesShellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMock('Cake\Console\ConsoleIo');
        $this->Dictionaries = new DictionariesShell($this->io);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dictionaries);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
