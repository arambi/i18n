<?php
namespace I18n\Test\TestCase\Shell;

use Cake\TestSuite\TestCase;
use I18n\Shell\ExtractNodesShell;

/**
 * I18n\Shell\ExtractNodesShell Test Case
 */
class ExtractNodesShellTest extends TestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
      'plugin.i18n.dictionaries',
      'plugin.i18n.languages',
      'plugin.i18n.translates',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $this->io = $this->getMock('Cake\Console\ConsoleIo');
      $this->ExtractNodes = new ExtractNodesShell($this->io);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset( $this->ExtractNodes);

      parent::tearDown();
  }

  /**
   * Test main method
   *
   * @return void
   */
  public function testMain()
  {
    $this->ExtractNodes->main();
  }
}
