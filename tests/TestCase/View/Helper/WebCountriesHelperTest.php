<?php
namespace I18n\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use I18n\View\Helper\WebCountriesHelper;

/**
 * I18n\View\Helper\WebCountriesHelper Test Case
 */
class WebCountriesHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \I18n\View\Helper\WebCountriesHelper
     */
    public $WebCountries;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->WebCountries = new WebCountriesHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WebCountries);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
